<?php

declare(strict_types=1);

namespace DKXTests\Psr7RequestBodyMapper\Entities;

use Symfony\Component\Validator\Constraints as Assert;

final class LabelBody
{
	/** @Assert\Type("string") */
	public ?string $title = null;
}
