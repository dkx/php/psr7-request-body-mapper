<?php

declare(strict_types=1);

namespace DKXTests\Psr7RequestBodyMapper\Entities;

use Symfony\Component\Validator\Constraints as Assert;

final class AddressBody
{
	/**
	 * @Assert\Type("string")
	 * @Assert\NotBlank
	 */
	public string $city;

	/**
	 * @Assert\Type("string")
	 * @Assert\NotBlank
	 */
	public string $street;

	/**
	 * @Assert\Type("int")
	 * @Assert\NotBlank
	 */
	public int $zipCode;
}
