<?php

declare(strict_types=1);

namespace DKXTests\Psr7RequestBodyMapper\Entities;

use Symfony\Component\Validator\Constraints as Assert;

final class UserBody
{
	/**
	 * @Assert\Type("string")
	 * @Assert\NotBlank
	 */
	public string $name;

	/**
	 * @Assert\Type("string")
	 * @Assert\NotBlank
	 * @Assert\Email
	 */
	public string $email;

	/**
	 * @Assert\Valid
	 * @Assert\NotBlank
	 */
	public AddressBody $address;

	/**
	 * @var LabelBody[]
	 * @Assert\Valid
	 */
	public array $labels = [];

	/**
	 * @var string[]|null
	 * @Assert\Type("array")
	 * @Assert\All({
	 *     @Assert\Type("string"),
	 *     @Assert\NotBlank
	 * })
	 */
	public ?array $roles = [];

	/**
	 * @var mixed[]|AddressBody|null
	 * @Assert\Valid
	 */
	public $invalidDocBlock;
}
