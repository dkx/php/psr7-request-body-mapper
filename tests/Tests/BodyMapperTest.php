<?php

declare(strict_types=1);

namespace DKXTests\Psr7RequestBodyMapper\Tests;

use DKX\Psr7RequestBodyMapper\BodyMapper;
use DKX\Psr7RequestBodyMapper\Exception\EmptyRequestDataException;
use DKX\Psr7RequestBodyMapper\Exception\InvalidRequestDataException;
use DKX\Psr7RequestBodyMapper\Exception\NotArrayRequestBodyException;
use DKXTests\Psr7RequestBodyMapper\Entities\UserBody;
use Mockery;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function assert;

final class BodyMapperTest extends TestCase
{
	private BodyMapper $bodyMapper;

	/** @var Mockery\MockInterface|ServerRequestInterface */
	private $request;

	public function setUp() : void
	{
		parent::setUp();

		$this->bodyMapper = new BodyMapper($this->createValidator());
		$this->request    = Mockery::mock(ServerRequestInterface::class);
	}

	public function testCreateDTOMissingData() : void
	{
		self::expectException(EmptyRequestDataException::class);
		self::expectExceptionMessage('HTTP request does not contain any data');

		$this->request->shouldReceive('getParsedBody')->andReturn(null);
		$this->bodyMapper->map($this->request, UserBody::class);
	}

	public function testCreateDTODataNotArray() : void
	{
		self::expectException(NotArrayRequestBodyException::class);
		self::expectExceptionMessage('HTTP request returned "object" but an array was expected');

		$this->request->shouldReceive('getParsedBody')->andReturn(new class {
        });
		$this->bodyMapper->map($this->request, UserBody::class);
	}

	public function testCreateDTOWithValidatorInvalid() : void
	{
		self::markTestSkipped('https://github.com/symfony/symfony/issues/35454');

		$this->request->shouldReceive('getParsedBody')->andReturn([]);

		try {
			$this->bodyMapper->map($this->request, UserBody::class);
		} catch (InvalidRequestDataException $e) {
			$errors = $e->getViolationsList();
			self::assertCount(3, $errors);
		}
	}

	public function testCreateDTO() : void
	{
		$this->request->shouldReceive('getParsedBody')->andReturn([
			'name' => 'John Doe',
			'email' => 'john@doe.com',
			'address' => [
				'city' => 'Prague',
				'street' => 'Lorem Ipsum 5',
				'zipCode' => 123456,
			],
			'labels' => [
				['title' => 'First'],
				['title' => 'Second'],
			],
			'roles' => ['normal', 'admin'],
		]);

		$entity = $this->bodyMapper->map($this->request, UserBody::class);
        assert($entity instanceof UserBody);

		self::assertSame('John Doe', $entity->name);
		self::assertSame('john@doe.com', $entity->email);
		self::assertSame('Prague', $entity->address->city);
		self::assertSame('Lorem Ipsum 5', $entity->address->street);
		self::assertSame(123456, $entity->address->zipCode);
		self::assertCount(2, $entity->labels);
		self::assertSame('First', $entity->labels[0]->title);
		self::assertSame('Second', $entity->labels[1]->title);
		self::assertEquals(['normal', 'admin'], $entity->roles);
	}

	private function createValidator() : ValidatorInterface
	{
		return Validation::createValidatorBuilder()
			->enableAnnotationMapping()
			->getValidator();
	}
}
