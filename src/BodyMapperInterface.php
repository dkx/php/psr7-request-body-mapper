<?php

declare(strict_types=1);

namespace DKX\Psr7RequestBodyMapper;

use Psr\Http\Message\ServerRequestInterface;

interface BodyMapperInterface
{
	public function map(ServerRequestInterface $request, string $entityClass) : object;
}
