<?php

declare(strict_types=1);

namespace DKX\Psr7RequestBodyMapper;

use DKX\Psr7RequestBodyMapper\Exception\EmptyRequestDataException;
use DKX\Psr7RequestBodyMapper\Exception\InvalidPropertyTypeException;
use DKX\Psr7RequestBodyMapper\Exception\InvalidRequestDataException;
use DKX\Psr7RequestBodyMapper\Exception\NotArrayRequestBodyException;
use Doctrine\Common\Annotations\AnnotationReader;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use phpDocumentor\Reflection\DocBlockFactory;
use phpDocumentor\Reflection\Types\Array_;
use phpDocumentor\Reflection\Types\Compound;
use phpDocumentor\Reflection\Types\ContextFactory;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionClass;
use ReflectionProperty;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function array_key_exists;
use function array_map;
use function assert;
use function count;
use function gettype;
use function in_array;
use function is_array;

final class BodyMapper implements BodyMapperInterface
{
	private ?ValidatorInterface $validator;

	private AnnotationReader $annotationReader;

	private DocBlockFactory $docBlockFactory;

	public function __construct(?ValidatorInterface $validator = null)
	{
		$this->annotationReader = new AnnotationReader();
		$this->docBlockFactory  = DocBlockFactory::createInstance([
			'var' => Var_::class,
		]);

		$this->validator = $validator;
	}

	public function map(ServerRequestInterface $request, string $entityClass) : object
	{
		$data = $request->getParsedBody();

		if ($data === null) {
			throw EmptyRequestDataException::create();
		}

		if (! is_array($data)) {
			throw NotArrayRequestBodyException::create(gettype($data));
		}

		return $this->createEntity($data, $entityClass);
	}

	/**
	 * @param mixed[] $data
	 */
	private function createEntity(array $data, string $entityClass) : object
	{
		$entity = new $entityClass();
		$ref    = new ReflectionClass($entity);
		$props  = $ref->getProperties(ReflectionProperty::IS_PUBLIC);

		foreach ($props as $prop) {
			if (! array_key_exists($prop->getName(), $data)) {
                continue;
            }

            $value = $data[$prop->getName()];

            if (is_array($value)) {
                $value = $this->getMappedValue($prop, $value);
            }

            $prop->setValue($entity, $value);
		}

		if ($this->validator !== null) {
			$errors = $this->validator->validate($entity);

			if (count($errors) > 0) {
				throw InvalidRequestDataException::create($entityClass, $errors);
			}
		}

		return $entity;
	}

	/**
     * @param mixed[] $value
     *
     * @return mixed
	 */
	private function getMappedValue(ReflectionProperty $prop, array $value)
	{
		$type = $prop->getType();

		if ($type === null) {
			throw InvalidPropertyTypeException::missingType($prop->getDeclaringClass()->getName(), $prop->getName());
		}

		if ($type->getName() === 'array') {
			$arrayType = $this->specifyArrayType($prop);
			if (in_array($arrayType, ['string', 'mixed', 'array', 'int', 'bool'], true)) {
				return $value;
			}

			return array_map(function ($arrayValue) use ($arrayType) {
				return $this->createEntity($arrayValue, $arrayType);
			}, $value);
		}

		if ($type->isBuiltin()) {
			return $value;
		}

		return $this->createEntity($value, $type->getName());
	}

	private function specifyArrayType(ReflectionProperty $prop) : string
	{
		$docBlock = $prop->getDocComment();

		if ($docBlock === false) {
			throw InvalidPropertyTypeException::unspecifiedArrayType($prop->getDeclaringClass()->getName(), $prop->getName());
		}

		$contextFactory = new ContextFactory();
		$doc            = $this->docBlockFactory->create($docBlock, $contextFactory->createFromReflector($prop));
		$vars           = $doc->getTagsByName('var');

		if (count($vars) !== 1) {
			throw InvalidPropertyTypeException::unspecifiedArrayType($prop->getDeclaringClass()->getName(), $prop->getName());
		}

		$var = $vars[0];
        assert($var instanceof Var_);
		$type = $var->getType();

		if ($type instanceof Compound) {
			foreach ($type as $subType) {
				if ($subType instanceof Array_) {
					/** @psalm-suppress LoopInvalidation */
					$type = $subType;
					break;
				}
			}
		}

		if (! ($type instanceof Array_)) {
			throw InvalidPropertyTypeException::unspecifiedArrayType($prop->getDeclaringClass()->getName(), $prop->getName());
		}

		return (string) $type->getValueType();
	}
}
