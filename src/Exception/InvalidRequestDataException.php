<?php

declare(strict_types=1);

namespace DKX\Psr7RequestBodyMapper\Exception;

use RuntimeException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use function method_exists;

final class InvalidRequestDataException extends RuntimeException
{
	private ConstraintViolationListInterface $violationsList;

	public function __construct(string $message, ConstraintViolationListInterface $violationsList)
	{
		parent::__construct($message);

		$this->violationsList = $violationsList;
	}

	public static function create(string $className, ConstraintViolationListInterface $violationsList) : self
	{
		$detail = '';

		/** @psalm-suppress InvalidCast */
		if (method_exists($violationsList, '__toString')) {
			$detail = "\n\n" . (string) $violationsList;
		}

		return new self($className . ': validation of mapped HTTP request data failed' . $detail, $violationsList);
	}

	public function getViolationsList() : ConstraintViolationListInterface
	{
		return $this->violationsList;
	}
}
