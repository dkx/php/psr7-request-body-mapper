<?php

declare(strict_types=1);

namespace DKX\Psr7RequestBodyMapper\Exception;

use RuntimeException;

final class EmptyRequestDataException extends RuntimeException
{
	public static function create() : self
	{
		return new self('HTTP request does not contain any data');
	}
}
