<?php

declare(strict_types=1);

namespace DKX\Psr7RequestBodyMapper\Exception;

use RuntimeException;

final class NotArrayRequestBodyException extends RuntimeException
{
	public static function create(string $receivedType) : self
	{
		return new self('HTTP request returned "' . $receivedType . '" but an array was expected');
	}
}
