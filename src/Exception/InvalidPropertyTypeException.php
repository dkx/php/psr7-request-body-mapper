<?php

declare(strict_types=1);

namespace DKX\Psr7RequestBodyMapper\Exception;

use InvalidArgumentException;

final class InvalidPropertyTypeException extends InvalidArgumentException
{
	public static function missingType(string $className, string $propertyName) : self
	{
		return new self($className . '::$' . $propertyName . ': missing type');
	}

	public static function unspecifiedArrayType(string $className, string $propertyName) : self
	{
		return new self($className . '::$' . $propertyName . ': array type must specify type of inner items in docblock (@var InnerTypeClass[])');
	}
}
