# DKX/PHP/PSR7 RequestBodyMapper

PSR7 request body mapper

## Installation

```bash
$ composer require dkx/psr7-request-body-mapper
```

## Usage

```php
<?php

use DKX\Psr7RequestBodyMapper\BodyMapper;
use Symfony\Component\Validator\Validation;

class UserEntity
{
    /**
     * @Assert\Type("string")
     * @Assert\NotBlank
     * @Assert\Email
     */
    public string $email;
    
    /**
     * @Assert\Type("string")
     * @Assert\NotBlank 
     */
    public string $password;
}

$validator = Validation::createValidatorBuilder()
    ->enableAnnotationMapping()
    ->getValidator();

$mapper = new BodyMapper($validator);

/** @var UserEntity $user */
$user = $mapper->map($request, UserEntity::class);
```
